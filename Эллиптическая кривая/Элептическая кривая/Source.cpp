#include <iostream>
#include <ctime>
using namespace std;
const int p = 211;
int a = 1;
int b = 7;
bool point[p][p];
int orderGroup = 0;
int orderSubgroup = 0;

int poModulu(int value)
{
	value %= p;
	while (value < 0)
		value += p;
	return value;
}

int factorization(int N)
{
	unsigned long long p, q, x, y, d = 1, k = floor(sqrt(N)) + 1;
	y = k * k - N;
	while (sqrt(y) != floor((sqrt(y))))
	{
		y = y + 2 * k + d;
		d += 2;
	}
	if (sqrt(y) >= N / 2)
		return - 1;
	x = floor(sqrt(N + y));
	y = floor(sqrt(y));
	p = x + y;
	q = x - y;
	return p > q ? p : q;
}

void findPoints()
{
	for (int x = 0; x < p; x++)
		for (int y = 0; y < p; y++)
		{
			if ((y * y) % p == (x * x * x + a * x + b) % p)
			{
				point[x][y] = true;
				orderGroup++;
			}
		}
}

int extendedEuclid(int a, int b)
{
	//��� ����� x
	int q, r, x1, x2, y1, y2, x, y, d;
	if (b == 0)
	{
		d = a;
		x = 1;
		y = 0;
		return - 1;
	}
	x2 = 1;
	x1 = 0;
	y2 = 0;
	y1 = 1;
	while (b > 0)
	{
		q = a / b;
		r = a - q * b;
		x = x2 - q * x1;
		y = y2 - q * y1;
		a = b;
		b = r;
		x2 = x1;
		x1 = x;
		y2 = y1;
		y1 = y;
	}
	d = a;
	x = x2;
	y = y2;
	return x;
}

int addPoint(int &x, int &y, int k = -1)
{
	int lambda;
	int x1 = x, y1 = y, x2 = x, y2 = y;
	int order = 1;
	while (true)
	{
		if (order == k)
		{
			x = x2;
			y = y2;
			return order;
		}
		if (x1 == x2 && y1 == y2)
		{
			if (y1 == 0)
				return ++order;
			lambda = poModulu((3 * x1 * x1 + a) * extendedEuclid(2 * y1, p));
		}
		else
		{
			int znamenatel = poModulu(x2 - x1);
			if (znamenatel == 0)
				return ++order;
			lambda = poModulu((y2 - y1) * extendedEuclid(znamenatel, p));
		}
		x2 = poModulu(lambda * lambda - x1 - x2);
		y2 = poModulu(lambda * (x1 - x2) - y1);
		order++;
	}
	return order;
}

void findPBPpoint(int &X, int &Y)
{
	for (int x = 0; x < p; x++)
		for (int y = 0; y < p; y++)
		{
			if (point[x][y])
			{
				int order = addPoint(x, y);
				if (order == orderSubgroup)
				{
					X = x;
					Y = y;
					return;
				}
			}
		}
}

void main()
{
	setlocale(LC_ALL, "rus");
	// + ����� �
	findPoints();
	++orderGroup;
	//cout << "������� ������: " << orderGroup  <<endl;
	orderSubgroup = factorization(orderGroup);
	//cout << "������� ���������: " << orderSubgroup << endl;
	//������� 13, ������ ��� ������ ��������� ��������� ������� 13
	int Px, Py; // ���������� ����� P
	findPBPpoint(Px, Py);
	//�������� ���� A
	srand(time(0));
	int PAx, PAy;
	int nA =  rand() % (orderSubgroup - 1);
	PAx = Px;
	PAy = Py;
	addPoint(PAx, PAy, nA);
	//�������� ���� B
	int PBx, PBy;
	int nB = rand() % (orderSubgroup - 1);
	PBx = Px;
	PBy = Py;
	addPoint(PBx, PBy, nB);
	
	int GA = addPoint(PBx, PBy, nA);
	int GB = addPoint(PAx, PAy, nB);
	cout << "���������� G � �����: " << PBx << " " << PBy << endl;
	cout << "���������� G � ����: " << PAx << " " << PAy << endl;
}