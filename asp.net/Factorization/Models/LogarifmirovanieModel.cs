﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factorization.Models
{
    public class LogarifmirovanieModel
    {
        public int y { get; set; }
        public int a { get; set; }
        public int n { get; set; }
    }
}