﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Numerics;

namespace Factorization.Help_Classes
{
    public class BinaryTreeNode<TNode> : IComparable
    where TNode : IComparable
    {
        public BinaryTreeNode(TNode value)
        {
            Value = value;
        }

        public BinaryTreeNode<TNode> Left { get; set; }
        public BinaryTreeNode<TNode> Right { get; set; }
        public TNode Value { get; private set; }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        /// 
        /// Сравнивает текущий узел с данным.
        /// 
        /// Сравнение производится по полю Value.
        /// Метод возвращает 1, если значение текущего узла больше,
        /// чем переданного методу, -1, если меньше и 0, если они равны
        public int CompareTo(TNode other)
        {
            return Value.CompareTo(other);
        }
    }

    public class BinaryTree : IEnumerable
    {
        private BinaryTreeNode<BigInteger> _head;
        private int _count;
        public List<BigInteger> Qs;
        public List<List<BigInteger>> QQs;
        public void PreOrderTraversal(BinaryTreeNode<BigInteger> node, SortedDictionary<BigInteger, Dictionary<int, int>> VectorList, Dictionary<int, int> CurrentSumVector)
        {
            if (node != null)
            {
                if (node.Value > 0)
                {
                    Qs.Add(node.Value);
                    Dictionary<int, int> vector = VectorList[node.Value];
                    foreach (var item in vector)
                    {
                        CurrentSumVector[item.Key] += item.Value;
                    }
                    bool bZero = true;
                    foreach (var item in CurrentSumVector)
                    {
                        if (item.Value % 2 == 1)
                        {
                            bZero = false;
                            break;
                        }
                    }
                    if (bZero == true)
                    {
                        QQs.Add(Qs);
                        Qs = new List<BigInteger>();
                        //return true;
                    }
                }
                PreOrderTraversal(node.Left, VectorList, CurrentSumVector);
                PreOrderTraversal(node.Right, VectorList, CurrentSumVector);
                if (node.Value > 0)
                {
                    if (Qs.Count > 0)
                        Qs.Remove(Qs.Last());
                    foreach (var item in VectorList[node.Value])
                    {
                        CurrentSumVector[item.Key] -= item.Value;
                    }
                }
            }
            //return false;
        }
        public void AddToLeafs(BigInteger left, BigInteger right)
        {
            PreOrderTraversal(_head, left, right);
        }

        public void CalcVectorInTree(SortedDictionary<BigInteger, Dictionary<int, int>> VectorList, List<int> FB)
        {
            Dictionary<int, int> CurrentSumVector = new Dictionary<int, int>();
            foreach (var item in FB)
                CurrentSumVector.Add(item, 0);
            Qs = new List<BigInteger>();
            QQs = new List<List<BigInteger>>();
            PreOrderTraversal(_head, VectorList, CurrentSumVector);
        }
        public void Add(BigInteger value)
        {
            // Случай 1: Если дерево пустое, просто создаем корневой узел.
            if (_head == null)
            {
                _head = new BinaryTreeNode<BigInteger>(value);
            }
            // Случай 2: Дерево не пустое => 
            // ищем правильное место для вставки.
            else
            {
                AddTo(_head, value);
            }

            _count++;
        }

        // Рекурсивная ставка.
        private void AddTo(BinaryTreeNode<BigInteger> node, BigInteger value)
        {
            // Случай 1: Вставляемое значение меньше значения узла
            if (value.CompareTo(node.Value) < 0)
            {
                // Если нет левого поддерева, добавляем значение в левого ребенка,
                if (node.Left == null)
                {
                    node.Left = new BinaryTreeNode<BigInteger>(value);
                }
                else
                {
                    // в противном случае повторяем для левого поддерева.
                    AddTo(node.Left, value);
                }
            }
            // Случай 2: Вставляемое значение больше или равно значению узла.
            else
            {
                // Если нет правого поддерева, добавляем значение в правого ребенка,
                if (node.Right == null)
                {
                    node.Right = new BinaryTreeNode<BigInteger>(value);
                }
                else
                {
                    // в противном случае повторяем для правого поддерева.
                    AddTo(node.Right, value);
                }
            }
        }

        public bool Contains(BigInteger value)
        {
            // Поиск узла осуществляется другим методом.
            BinaryTreeNode<BigInteger> parent;
            return FindWithParent(value, out parent) != null;
        }

        /// 
        /// Находит и возвращает первый узел с заданным значением. Если значение
        /// не найдено, возвращает null. Также возвращает родителя найденного узла (или null)
        /// для использования в методе Remove.
        /// 
        private BinaryTreeNode<BigInteger> FindWithParent(BigInteger value, out BinaryTreeNode<BigInteger> parent)
        {
            // Попробуем найти значение в дереве.
            BinaryTreeNode<BigInteger> current = _head;
            parent = null;

            // До тех пор, пока не нашли...
            while (current != null)
            {
                int result = current.CompareTo(value);

                if (result > 0)
                {
                    // Если искомое значение меньше, идем налево.
                    parent = current;
                    current = current.Left;
                }
                else if (result < 0)
                {
                    // Если искомое значение больше, идем направо.
                    parent = current;
                    current = current.Right;
                }
                else
                {
                    // Если равны, то останавливаемся
                    break;
                }
            }

            return current;
        }

        public bool Remove(BigInteger value)
        {
            BinaryTreeNode<BigInteger> current, parent;

            // Находим удаляемый узел.
            current = FindWithParent(value, out parent);

            if (current == null)
            {
                return false;
            }

            _count--;

            // Случай 1: Если нет детей справа, левый ребенок встает на место удаляемого.
            if (current.Right == null)
            {
                if (parent == null)
                {
                    _head = current.Left;
                }
                else
                {
                    int result = parent.CompareTo(current.Value);
                    if (result > 0)
                    {
                        // Если значение родителя больше текущего,
                        // левый ребенок текущего узла становится левым ребенком родителя.
                        parent.Left = current.Left;
                    }
                    else if (result < 0)
                    {
                        // Если значение родителя меньше текущего, 
                        // левый ребенок текущего узла становится правым ребенком родителя. 
                        parent.Right = current.Left;
                    } } }
            // Случай 2: Если у правого ребенка нет детей слева 
            // то он занимает место удаляемого узла. 
            else if (current.Right.Left == null)
            { current.Right.Left = current.Left;
                if (parent == null)
                { _head = current.Right; }
                else { int result = parent.CompareTo(current.Value); if (result > 0)
                        {
                            // Если значение родителя больше текущего,
                            // правый ребенок текущего узла становится левым ребенком родителя.
                            parent.Left = current.Right;
                        }
            else if (result < 0)
                        {
                        // Если значение родителя меньше текущего, 
                        // правый ребенок текущего узла становится правым ребенком родителя. 
                        parent.Right = current.Right; } } }
            // Случай 3: Если у правого ребенка есть дети слева, крайний левый ребенок 
            // из правого поддерева заменяет удаляемый узел. 
            else {
                // Найдем крайний левый узел. 
                BinaryTreeNode<BigInteger> leftmost = current.Right.Left;
                BinaryTreeNode<BigInteger> leftmostParent = current.Right;
                while (leftmost.Left != null) { leftmostParent = leftmost; leftmost = leftmost.Left; }
                // Левое поддерево родителя становится правым поддеревом крайнего левого узла. 
                leftmostParent.Left = leftmost.Right;
                // Левый и правый ребенок текущего узла становится левым и правым ребенком крайнего левого. 
                leftmost.Left = current.Left; leftmost.Right = current.Right; if (parent == null)
                { _head = leftmost; } else { int result = parent.CompareTo(current.Value); if (result > 0)
                            {
                                // Если значение родителя больше текущего,
                                // крайний левый узел становится левым ребенком родителя.
                                parent.Left = leftmost;
                            }
            else if (result < 0)
                            {
                                // Если значение родителя меньше текущего,
                                // крайний левый узел становится правым ребенком родителя.
                                parent.Right = leftmost;
                            }
                        }
                    }

                    return true;
                }

        private void PreOrderTraversal(BinaryTreeNode<BigInteger> node, BigInteger left, BigInteger right)
        {
            if (node != null)
            {
                PreOrderTraversal(node.Left, left, right);
                PreOrderTraversal(node.Right, left, right);
                if (node.Left == null && node.Right == null)
                {
                    BinaryTreeNode<BigInteger> leftNode = new BinaryTreeNode<BigInteger>(left);
                    BinaryTreeNode<BigInteger> rightNode = new BinaryTreeNode<BigInteger>(right);
                    node.Left = leftNode;
                    node.Right = rightNode;
                }
            }
        }
        public void PreOrderTraversal(Action<BigInteger> action)
        {
            PreOrderTraversal(action, _head);
        }

        private void PreOrderTraversal(Action<BigInteger> action, BinaryTreeNode<BigInteger> node)
        {
            if (node != null)
            {
                action(node.Value);
                PreOrderTraversal(action, node.Left);
                PreOrderTraversal(action, node.Right);
            }
        }

        public void PostOrderTraversal(Action<BigInteger> action)
        {
            PostOrderTraversal(action, _head);
        }

        private void PostOrderTraversal(Action<BigInteger> action, BinaryTreeNode<BigInteger> node)
        {
            if (node != null)
            {
                PostOrderTraversal(action, node.Left);
                PostOrderTraversal(action, node.Right);
                action(node.Value);
            }
        }

        public void InOrderTraversal(Action<BigInteger> action)
        {
            InOrderTraversal(action, _head);
        }

        private void InOrderTraversal(Action<BigInteger> action, BinaryTreeNode<BigInteger> node)
        {
            if (node != null)
            {
                InOrderTraversal(action, node.Left);

                action(node.Value);

                InOrderTraversal(action, node.Right);
            }
        }

        public IEnumerator InOrderTraversal()
        {
            // Это нерекурсивный алгоритм.
            // Он использует стек для того, чтобы избежать рекурсии.
            if (_head != null)
            {
                // Стек для сохранения пропущенных узлов.
                Stack<BinaryTreeNode<BigInteger>> stack = new Stack<BinaryTreeNode<BigInteger>>();

                BinaryTreeNode<BigInteger> current = _head;

                // Когда мы избавляемся от рекурсии, нам необходимо
                // запоминать, в какую стороны мы должны двигаться.
                bool goLeftNext = true;

                // Кладем в стек корень.
                stack.Push(current);

                while (stack.Count > 0)
                {
                    // Если мы идем налево...
                    if (goLeftNext)
                    {
                        // Кладем все, кроме самого левого узла на стек.
                        // Крайний левый узел мы вернем с помощю yield.
                        while (current.Left != null)
                        {
                            stack.Push(current);
                            current = current.Left;
                        }
                    }

                    // Префиксный порядок: left->yield->right.
                    yield return current.Value;

                    // Если мы можем пойти направо, идем.
                    if (current.Right != null)
                    {
                        current = current.Right;

                        // После того, как мы пошли направо один раз,
                        // мы должным снова пойти налево.
                        goLeftNext = true;
                    }
                    else
                    {
                        // Если мы не можем пойти направо, мы должны достать родительский узел
                        // со стека, обработать его и идти в его правого ребенка.
                        current = stack.Pop();
                        goLeftNext = false;
                    }
                }
            }
        }

        public IEnumerator GetEnumerator()
        {
            return InOrderTraversal();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            _head = null;
            _count = 0;
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }
    }
}