﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorization.Help_Classes
{
    public static class Gauss
    {
        public static List<BitArray> buildGaussMatrix(List<List<int>> V)
        {
            List<BitArray> GMatrix = new List<BitArray>();
            
            for (int positionCounter = 0; positionCounter < V.Count; positionCounter++)
            {
                BitArray g = new BitArray(V[0].Count + V.Count, false);
                //копируем в новый массив старые вектора
                for (int i = 0; i < V[positionCounter].Count; i++)
                {
                    g.Set(i, V[positionCounter][i] % 2 == 0);
                }
                //добавляем справа единичную матрицу
                g.Set(V[positionCounter].Count + positionCounter, true);
                GMatrix.Add(g);
            }
            GMatrix = buildTriangleMatrix(GMatrix);
            return GMatrix;
        }

        private static List<BitArray> buildTriangleMatrix(List<BitArray> GMatrix)
        {
            int n = GMatrix[0].Count - GMatrix.Count;
            //пока i<n
            int i = 0;
            while (i < n)
            {
                //найти вектор с 1 в i-ом столбце
                List<int> positions = findVectorWith1(i, GMatrix);
                //если нет - к следующему столбцу
                if (positions.Count < 1) i++;
                //если есть: поднять его наверх на i-ую позицию, вычесть из остальных векторов с 1 в этом столбце, перейти к следующему столбцу
                else
                {
                    for (int j = 1; j<positions.Count; j++)
                    {
                        GMatrix = Subtract(positions[j], positions[0], GMatrix);
                    }
                    GMatrix = UpVector(positions[0], i, GMatrix);                    
                    i++;
                }
            }
            return GMatrix;
        }

        private static List<BitArray> Subtract(int umenshaemoe, int vychitaemoe, List<BitArray> GMatrix)
        {
            for (int i = 0; i < GMatrix[umenshaemoe].Count; i++)
            {
                if (GMatrix[vychitaemoe][i])
                {
                    GMatrix[umenshaemoe][i] = !GMatrix[umenshaemoe][i];
                }
            }
            return GMatrix;
        }

        private static List<BitArray> UpVector (int now, int shouldbe, List<BitArray> GMatrix)
        {
            if (now != shouldbe)
            {
                BitArray temp = new BitArray(GMatrix[now]);
                GMatrix[now] = GMatrix[shouldbe];
                GMatrix[shouldbe] = temp;
            }
            return GMatrix;
        }

        private static List<int> findVectorWith1(int column, List<BitArray> GMatrix)
        {
            List<int> positions = new List<int>();
            for (int i = column; i < GMatrix.Count; i++)
            {
                if (GMatrix[i].Get(column) == true)
                {
                    positions.Add(i);
                }
            }
            return positions;
        }

        public static List<List<int>> getVectorsNumbers(List<BitArray> GMatrix)
        {
            List<List<int>> numbers = new List<List<int>>();
            for (int row = GMatrix[0].Count - GMatrix.Count; row < GMatrix.Count; row++)
            {
                List<int> variant = new List<int>();
                for (int column = GMatrix[0].Count - GMatrix.Count; column < GMatrix[row].Count; column++)
                {
                    if (GMatrix[row][column])
                    {
                        variant.Add(column - (GMatrix[0].Count - GMatrix.Count));
                    }
                }
                if (variant.Count>0)
                {
                    numbers.Add(variant);
                }
            }
            return numbers;
        }
    }
}
