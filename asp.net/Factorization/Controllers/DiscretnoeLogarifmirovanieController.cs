﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Factorization.Models;
using System.Numerics;

namespace Factorization.Controllers
{
    public class DiscretnoeLogarifmirovanieController : Controller
    {
        private int DiscretnoeLogarifmirovanie(LogarifmirovanieModel model)
        {
            int s = Int32.Parse(Math.Floor(Math.Sqrt(model.n)).ToString());
            //маленький шаг
            SortedDictionary<BigInteger, int> S = new SortedDictionary<BigInteger, int>();
            for (int i = 0; i < s; i++)
            {
                BigInteger bi = BigInteger.ModPow(model.y * BigInteger.Pow(model.a, i), 1, model.n);
                if (!S.ContainsKey(bi))
                    S.Add(bi, i);
            }
            //большой шаг
            SortedDictionary<BigInteger, int> T = new SortedDictionary<BigInteger, int>();
            for (int i = 1; i <= s; i++)
            {
                BigInteger bi = BigInteger.ModPow(model.a, i * s, model.n);
                if (!T.ContainsKey(bi))
                    T.Add(bi, (i * s) % model.n);
            }

            foreach (var itemS in S)
            {
                foreach (var itemT in T)
                {
                    if (itemT.Key > itemS.Key)
                        break;

                    if (itemT.Key == itemS.Key)
                        return Math.Abs(itemT.Value - itemS.Value);
                }
            }
            return -1;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string Index(LogarifmirovanieModel model)
        {
            int answer = DiscretnoeLogarifmirovanie(model);
            return answer.ToString();
        }
    }
}