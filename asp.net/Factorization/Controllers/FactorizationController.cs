﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Numerics;
using Factorization.Models;
using Factorization.Help_Classes;
using System.Collections;

namespace Factorization.Controllers
{
    static class CalcBigInteger
    {
        public static BigInteger GCD(BigInteger a, BigInteger b)
        {
            return b == 0 ? a : GCD(b, a % b);
        }
        public static BigInteger Sqrt(this BigInteger n)
        {
            if (n == 0) return 0;
            if (n > 0)
            {
                int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
                BigInteger root = BigInteger.One << (bitLength / 2);

                while (!isSqrt(n, root))
                {
                    root += n / root;
                    root /= 2;
                }

                return root;
            }

            throw new ArithmeticException("NaN");
        }

        private static Boolean isSqrt(BigInteger n, BigInteger root)
        {
            BigInteger lowerBound = root * root;
            BigInteger upperBound = (root + 1) * (root + 1);

            return (n >= lowerBound && n < upperBound);
        }
    }
    class SignLejanra
    {
        BigInteger number;

        public SignLejanra()
        {
        }

        public SignLejanra(BigInteger value)
        {
            number = value;
        }

        public bool IsVichet(int p)
        {
            bool result = true;
            BigInteger value = BigInteger.ModPow(number, (p - 1) / 2, p);
            if (value == p - 1)
                result = false;
            //Dictionary<int, int> dictionary = new Dictionary<int, int>();
            //q = q % p;
            //BigInteger b = q;
            //int div = 2;
            //while (b > 1)
            //{
            //    while (b % div == 0)
            //    {
            //        if (dictionary.ContainsKey(div))
            //        {
            //            if (++dictionary[div] == 2)
            //                dictionary.Remove(div);
            //        }
            //        else
            //            dictionary.Add(div, 1);
            //        b /= div;
            //    }
            //    div++;
            //}
            //double result = 1;
            //foreach (var item in dictionary)
            //{
            //    double vichet = 0;
            //    if (item.Key == 2)
            //        vichet = Math.Pow(-1, (p * p - 1) / 8);
            //    else
            //    {
            //        number = p;
            //        vichet = IsVichet(item.Key) * Math.Pow(-1, ((p - 1) / 2) * ((item.Key - 1) / 2));
            //    }
            //    result *= vichet;
            //}
            return result;
        }
    }
    public class FactorizationController : Controller
    {
        BigInteger N;
        BigInteger SqrtN;
        SignLejanra Lejanr = null;
        List<int> FB = null;
        BigInteger p;
        BigInteger q;
        private Dictionary<int, bool> Eratosfen(int B)
        {
            Dictionary<int, bool> eratosfen = new Dictionary<int, bool>();
            for (int i = 1; i <= B; i++)
                eratosfen.Add(i, true);
            eratosfen[1] = false;
            for (int i = 2; i * i <= B; i++)
            {
                if (eratosfen[i] == true)
                    for (int j = i * i; j <= B; j += i)
                        eratosfen[j] = false;
            }
            return eratosfen;
        }
        private List<int> createFB(int B)
        {
            Dictionary<int, bool> eratosfen = Eratosfen(B);
            FB = new List<int>();
            FB.Add(-1);
            FB.Add(2);
            for (int i = 3; i <= B; i++)
            {
                if (eratosfen[i])
                    if (Lejanr.IsVichet(i))
                        FB.Add(i);
            }   
            return FB;
        }
        private string Calc()
        {
            Lejanr = new SignLejanra(N);
            FB = createFB(55000);
            List<List<int>> matr = new List<List<int>>();
            List<BigInteger> Qs = new List<BigInteger>();

            BigInteger param = SqrtN;
            int i = 1;
            for (int j = 0; j < 10000; j++)
            {
                param += i;
                Qs.Add(param);
                matr.Add(CalcQ(param));
                i = (Math.Abs(i) + 1) * Math.Sign(i) * -1;
            }
           
            List<BitArray> G = Gauss.buildGaussMatrix(matr);
            List < List < int>> vectorNumbersList = Gauss.getVectorsNumbers(G);

            foreach (List<int> vectorNumbers in vectorNumbersList)
            {
                BigInteger xKvadrat = 1, yKvadrat = 1;
                foreach (int number in vectorNumbers)
                {
                    xKvadrat *= Qs[number];
                    for (int k = 0; k < matr[number].Count; k++)
                    {
                        if (matr[number][k] > 0)
                            yKvadrat *= BigInteger.Pow(FB[k], matr[number][k]);
                    }
                }

                yKvadrat = CalcBigInteger.Sqrt(yKvadrat);
                xKvadrat %= N;
                yKvadrat %= N;
                if (BigInteger.ModPow(xKvadrat, 2, N) == BigInteger.ModPow(yKvadrat, 2, N) && yKvadrat != xKvadrat)
                {
                    p = CalcBigInteger.GCD(BigInteger.Abs(xKvadrat - yKvadrat), N);
                    if (p == 1)
                    {
                        q = CalcBigInteger.GCD(BigInteger.Abs(xKvadrat + yKvadrat), N);
                        p = N / q;
                    }
                    else
                        q = N / p;
                    if (p != 1 && q != 1)
                        break;
                }
            }
            string result = "p = " + p.ToString() + "\nq = " + q.ToString();
            return result;
        }
        private List<int> CalcQ(BigInteger value)
        {
            BigInteger Q = BigInteger.Pow(value, 2) - N;
            List<int> vector = new List<int>();
            for (int i = 0; i < FB.Count; i++)
            {
                vector.Add(0);
                if (FB[i] == -1)
                {
                    if (Q < 0)
                        vector[i]++;
                }
                else
                {
                    while (Q % FB[i] == 0)
                    {
                        vector[i]++;
                        Q /= FB[i];
                    }
                }
            }
            return vector;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string Index(BigIntegerModel inputModel)
        {
            N = BigInteger.Parse(inputModel.inputString);
            SqrtN = CalcBigInteger.Sqrt(N);
            return Calc();
        }

    }
}