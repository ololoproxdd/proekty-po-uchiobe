#include <NTL/ZZ.h>
#include <iostream>
#include <list>
#include <iterator> // ��������� ����������
#include <vector>
#include <cmath>
#include <NTL\ZZ.h>
NTL_CLIENT
//�������� �������. �������� ��� ���������� �����������
//������ �������� ���� ����� �����. ������������ ��� ��������
//����� �� ��������������.
ZZ gcd(ZZ a, ZZ b)
{
	ZZ c;
	while (b > 0)
	{
		c = a % b;
		a = b;
		b = c;
	}
	return a;
}

//�������� �� ������
//���������� a * x + b * y = gcd(a, b) = d
void extendedEuclid(ZZ a, ZZ b, ZZ &x, ZZ &y, ZZ &d)
{
	ZZ q, r, x1, x2, y1, y2;
	if (b == 0)
	{
		d = a;
		x = 1;
		y = 0;
		return;
	}
	x2 = 1;
	x1 = 0;
	y2 = 0;
	y1 = 1;
	while (b > 0)
	{
		q = a / b;
		r = a - q * b;
		x = x2 - q * x1;
		y = y2 - q * y1;
		a = b;
		b = r;
		x2 = x1;
		x1 = x;
		y2 = y1;
		y1 = y;
	}
	d = a;
	x = x2;
	y = y2;
}

//���������� ��������� ����� a �� ������ n
ZZ calculateD(ZZ a, ZZ n)
{
	ZZ d, x, y;
	extendedEuclid(a, n, x, y, d);
	if (d != 1)
		x = 0;
	if (x < 0)
		x = n + x;
	return x;
}

ZZ functionEuler(ZZ p, ZZ q)
{
	return (q - 1) * (p - 1);
}

ZZ calculateE(ZZ p, ZZ q, ZZ euler)
{
	ZZ max;
	if (p > q)
		max = p;
	else
		max = q;
	while (gcd(++max, euler) != 1);
	return max;
}

string readFile()
{
	ifstream input;
	input.open("input.txt");
	string strWord;
	string strText;
	while (!input.eof())
	{
		if (!strText.empty())
			strText += " ";
		input >> strWord;
		strText += strWord;
	}
	input.close();
	return strText;
}

void writeFile(string strEncrypt)
{
	ofstream output;
	output.open("output.txt");
	output << strEncrypt;
	output.close();
}

bool ferma(ZZ x)
{
	if (x == 2)
		return true;
	for (int i = 0; i < 100; i++)
	{
		ZZ a = AddMod((ZZ)0, rand(), x - 2) + 2;
		if (gcd(a, x) != 1)
			return false;
		if (PowerMod(a, x - 1, x) != 1)
			return false;
	}
	return true;

}
void generatePQ(long n, ZZ& p, ZZ& q)
{
	ZZ mod1 = power((ZZ)10, n);
	ZZ mod = power((ZZ)10, n+1);
	time_t t;
	srand((unsigned)&t);
	p = AddMod(mod1, rand(), mod - mod1);
	q = AddMod(mod1, rand(), mod - mod1);
	while (!ferma(++p));
	while (!ferma(++q));
}

void main()
{
	ZZ *arrayDecrypt = nullptr;
	ZZ *arrayEncrypt = nullptr;
	setlocale(LC_ALL, "rus");
	ZZ q, p, n, euler, e, d, N;
	long k;
	N = 256;
	// p = 11117, q = 11159;
	int poryadok = 40;
	generatePQ(poryadok, p, q);
	cout << "p : " << p << endl;
	cout << "q : " << q << endl;
	p = 3001;
	q = 2999;
	n = 8999999;//n = p * q;
	cout << "n : " << n << endl;
	k = (log(n) / log(N));
	cout << "������ ���������� ����� : " << k << endl;
	euler = functionEuler(p, q);
	e = 2141;
	//e = calculateE(p, q, euler);
	d = calculateD(e, euler);
	cout << "d: " << d << endl;
	cout << endl << "�������� ����� :" << endl;
	string strDecrypt;
	getline(cin, strDecrypt);
	size_t length = strDecrypt.length();
	arrayDecrypt = new ZZ[length];
	arrayEncrypt = new ZZ[length];
	cout << endl << "������������� ����� :" << endl;
	string encryptText;
	for (long i = 0; i < length; i = i + k)
	{
		ZZ a;
		long j = 0;
		while (j < k && i + j < length)
		{
			ZZ asciicode(strDecrypt[i + j]);
			a += asciicode*power(N, j);
			j++;
		}
		//cout << "Text: "<< a;
		arrayEncrypt[i] = PowerMod(a, e, n);
		ZZ z = arrayEncrypt[i];
		j = k - 1;
		string strEnc;
		while (j > -1)
		{
			a = z / power(N, j);
			z %= power(N, j);
			int c;
			conv(c, a);
			strEnc = (char)c + strEnc;
			j--;
		}
		encryptText += strEnc;
		//cout << " Enc: "<< arrayEncrypt[i] << endl;
	}
	cout << encryptText;

	cout << endl << endl << "�������������� ����� :" << endl;
	string decryptText;
	for (long i = 0; i < length; i = i + k)
	{
		arrayDecrypt[i] = PowerMod(arrayEncrypt[i], d, n);
		ZZ a;
		long j = k - 1;
		string strDec;
		while (j > -1)
		{
			a = arrayDecrypt[i] / power(N, j);
			arrayDecrypt[i] -= a*power(N, j);
			int c;
			conv(c, a);
			strDec = (char)c + strDec;
			j--;
		}
		decryptText += strDec;
	}
	cout << decryptText << endl;
	std::system("pause");
}