﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AtakaMetodomNepodvijnoiTochki
{
    class Program
    {
        static void Main(string[] args)
        {
            int e = 17;
            int N = 2773;
            int C = 2342;
            BigInteger storage = 0;
            for (int k = 1; k <= 100; k ++)
            {
                BigInteger A = BigInteger.ModPow(C, BigInteger.Pow(e, k), N);
                if (A == C)
                {
                    Console.WriteLine(storage);
                    return;
                }
                storage = A;
            }
        }
    }
}
