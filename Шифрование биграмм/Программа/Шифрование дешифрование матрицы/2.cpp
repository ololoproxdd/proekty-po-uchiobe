#include <iostream>
#include <string>
using namespace std; 
//					 �������: 3 1
//							  8 7
int g_arrayMatrica[2][2] = { {3,1}, 
							 {8,7} };
int g_reverseArrayMatrica[2][2];
const int g_nN = 27;
string g_alphabet[g_nN] = { "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"," "};
string g_strText = "mustaev ravil elvirovich perm psu kmb";
int **g_arrayText = nullptr;
int **g_arrayTextEncrypted = nullptr;
int **g_arrayTextDecrypted = nullptr;
int g_nArrayCount;

void dynamicMemory()
{
	g_arrayText = new int*[2];
	for (int i = 0; i < 2; i++)
		g_arrayText[i] = new int[g_nArrayCount];

	g_arrayTextEncrypted = new int*[2];
	for (int i = 0; i < 2; i++)
		g_arrayTextEncrypted[i] = new int[g_nArrayCount];

	g_arrayTextDecrypted = new int*[2];
	for (int i = 0; i < 2; i++)
		g_arrayTextDecrypted[i] = new int[g_nArrayCount];
}

int findNumber(char cSymbol)
{
	string strSymbol(1, cSymbol);
	for (int i = 0; i < g_nN; i++)
		if (strSymbol == g_alphabet[i])
			return i;
	return -1;
}

string mergeMatrix(int **arrayA, int **arrayB, int arrayC[][2])
{
	string strResult;
	for (int i = 0; i < g_nArrayCount; i++)
	{
		arrayA[0][i] = (arrayB[0][i] * arrayC[0][0] + arrayB[1][i] * arrayC[0][1]) % g_nN;
		arrayA[1][i] = (arrayB[0][i] * arrayC[1][0] + arrayB[1][i] * arrayC[1][1]) % g_nN;
		while (arrayA[0][i] < 0)
			arrayA[0][i] += g_nN;
		while (arrayA[1][i] < 0)
			arrayA[1][i] += g_nN;
		strResult += g_alphabet[arrayA[0][i]] + g_alphabet[arrayA[1][i]];
	}
	return strResult;
}

string encryption()
{
	for (int i = 0; i < g_nArrayCount; i++)
	{
		g_arrayText[0][i] = findNumber(g_strText[2 * i]);
		g_arrayText[1][i] = findNumber(g_strText[2 * i + 1]);
	}

	return mergeMatrix(g_arrayTextEncrypted, g_arrayText, g_arrayMatrica);
}

int matrixDeterminant()
{
	return g_arrayMatrica[0][0] * g_arrayMatrica[1][1] - g_arrayMatrica[0][1] * g_arrayMatrica[1][0];
}

bool reverseMatrix()
{
	int nDeterminant = matrixDeterminant();
	int nReverseDeterminant = 25;
	bool bResult = false;
	if (nDeterminant != 0 && nReverseDeterminant > 0)
	{
		g_reverseArrayMatrica[0][0] = g_arrayMatrica[1][1] * nReverseDeterminant;
		g_reverseArrayMatrica[0][1] = -g_arrayMatrica[0][1] * nReverseDeterminant;
		g_reverseArrayMatrica[1][0] = -g_arrayMatrica[1][0] * nReverseDeterminant;
		g_reverseArrayMatrica[1][1] = g_arrayMatrica[0][0] * nReverseDeterminant;
		bResult = true;
	}
	return bResult;
}

string decryption()
{
	string strResult;
	if (reverseMatrix())
		strResult = mergeMatrix(g_arrayTextDecrypted, g_arrayTextEncrypted, g_reverseArrayMatrica);
	return strResult;
}

void deleteDynamicStorage(int **array)
{
	for (int i = 0; i < 2; i++)
	{
		delete[] array[i];
	}
	delete[] array;
}

void main()
{
	setlocale(LC_ALL, "Rus");
	size_t nLength = g_strText.length();
	g_nArrayCount = nLength / 2;
	if (nLength % 2 != 0)
		g_nArrayCount++;

	dynamicMemory();
	cout << "������������� �����: " << endl;
	cout << encryption() << endl;
	cout << "�������������� �����: " << endl;
	cout << decryption() << endl;
	deleteDynamicStorage(g_arrayText);
	deleteDynamicStorage(g_arrayTextEncrypted);
	deleteDynamicStorage(g_arrayTextDecrypted);
}