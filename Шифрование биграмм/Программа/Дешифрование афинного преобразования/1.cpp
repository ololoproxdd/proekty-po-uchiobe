#include <iostream>
#include <string>
using namespace std;
string g_strInput = "jbyizjcygympcruagruoyuzrzp";
int g_nN = 26;
int g_nAReverse = 193;
int g_nB = 637;
string g_alphabet[26] = { "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z" };

int findNumber(char cSymbol)
{
	string strSymbol(1, cSymbol);
	for (int i = 0; i < 26; i++)
		if (strSymbol == g_alphabet[i])
			return i;
	return -1;
}

string decryption()
{
	//p = c * a^-1 - b* a^-1 (mod N)
	int nCount = g_strInput.length() / 2;
	int nC;
	int nP;
	string strResult;
	int nLetter1;
	int nLetter2;
	for (int i = 0; i < nCount; i++)
	{
		nC = findNumber(g_strInput[i]) * g_nN + findNumber(g_strInput[i + 1]);
		nP = (nC * g_nAReverse - g_nB * g_nAReverse) % (g_nN * g_nN);
		while (nP < 0)
			nP += g_nN * g_nN;
		nLetter1 = nP / g_nN;
		nLetter2 = nP % g_nN;
		strResult += g_alphabet[nLetter1] + g_alphabet[nLetter2];
	}
	return strResult;
}

void main()
{
	setlocale(LC_ALL, "Rus");
	cout << "������������� ������: " << decryption() << endl;
}