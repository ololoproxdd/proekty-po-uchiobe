﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace LogarifmirovanieSPH
{
    class Logarifmiravanie
    {
        int q;
        int a;
        int b;
        public Logarifmiravanie()
        {

        }
        public Logarifmiravanie(int q, int a, int b)
        {
            this.q = q;
            this.a = a;
            this.b = b;
        }
        public List<int> Eratosfen(int B)
        {
            Dictionary<int, bool> eratosfen = new Dictionary<int, bool>();
            for (int i = 1; i <= B; i++)
                eratosfen.Add(i, true);
            eratosfen[1] = false;

            for (int i = 2; i * i <= B; i++)
            {
                if (eratosfen[i])
                    for (int j = i * i; j <= B; j += i)
                        eratosfen[j] = false;
            }

            List<int> vector = new List<int>();
            for (int i = 2; i <= B; i++)
                if (eratosfen[i])
                    vector.Add(i);
            return vector;
        }
        public Dictionary<int, int> RazlojnieNaProstieMnojiteli(List<int> ProstieMnojiteli)
        {
            int value = q - 1;
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            int i = 0;
            while (value > 1)
            {
                int ProstoiMnojitel = ProstieMnojiteli[i];
                if (value % ProstoiMnojitel == 0)
                {
                    if (dictionary.ContainsKey(ProstoiMnojitel))
                        dictionary[ProstoiMnojitel]++;
                    else
                        dictionary.Add(ProstoiMnojitel, 1);
                    value /= ProstoiMnojitel;
                }
                else
                    i++;
            }
            return dictionary;
        }
        public Dictionary<int, List<BigInteger>> CreateTable(Dictionary<int, int> MnojitelAndStepen)
        {
            Dictionary<int, List<BigInteger>> TablicaP = new Dictionary<int, List<BigInteger>>();
            foreach (var item in MnojitelAndStepen)
            {
                int key = item.Key;
                List<BigInteger> StrokaTablici = new List<BigInteger>();
                for (int i = 0; i < key; i++)
                {
                    BigInteger r = BigInteger.ModPow(a, i * (q - 1) / key, q);
                    StrokaTablici.Add(r);
                }
                TablicaP.Add(key, StrokaTablici);
            }
            return TablicaP;
        }
        public int NomerJ(List<BigInteger> ListJ, BigInteger Rpj)
        {
            int i = 0;
            foreach (var item in ListJ)
            {
                if (item == Rpj)
                    return i;
                i++;
            }
            return -1;
        }
        public Dictionary<int, int> KitaiskayaTeorema(Dictionary<int, int> MnojitelAndStepen, Dictionary<int, List<BigInteger>> TablicaP)
        {
            Dictionary<int, int> KitaiskayaTeoremaSistema = new Dictionary<int, int>();
            foreach (var item in MnojitelAndStepen)
            {
                int value = item.Value;
                int key = item.Key;
                //xi = b^((q-1)/pi) mod q
                int StepenA = 0;
                int Z = 0;
                for (int i = 0; i < item.Value; i++)
                {
                    int B;
                    if (StepenA < 0)
                        B = Convert.ToInt32(b * ObratnoePoModulu(Convert.ToInt32(Math.Pow(a, Math.Abs(StepenA))), q));
                    else
                        B = Convert.ToInt32(b * Math.Pow(a, Math.Abs(StepenA)));
                    BigInteger Rpj = BigInteger.ModPow(B, (q - 1) / Convert.ToInt32(Math.Pow(key, i + 1)), q);
                    List<BigInteger> ListJ = TablicaP[key];
                    int XLast = NomerJ(ListJ, Rpj);
                    Z += XLast * Convert.ToInt32(Math.Pow(key, i));
                    StepenA += -1 * XLast * Convert.ToInt32(Math.Pow(key, i));
                }
                KitaiskayaTeoremaSistema.Add(Convert.ToInt32(Math.Pow(key, value)), Z);
            }
            return KitaiskayaTeoremaSistema;
        }
        public int ObratnoePoModulu(int a, int b)
        {
            //Вычисление a * x + b * y = gcd(a, b) = d
            int q, r, x1, x2, y1, y2, y, d, x;
            if (b == 0)
            {
                d = a;
                x = 1;
                y = 0;
                return x;
            }
            x2 = 1;
            x1 = 0;
            y2 = 0;
            y1 = 1;
            while (b > 0)
            {
                q = a / b;
                r = a - q * b;
                x = x2 - q * x1;
                y = y2 - q * y1;
                a = b;
                b = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }
            d = a;
            x = x2;
            y = y2;
            return x;
        }
        public int ReshenieKitayskoiTeoremi(Dictionary<int, int> KitaiskayaTeorema)
        {
            int M = 1;
            foreach (var item in KitaiskayaTeorema)
            {
                M *= item.Key;
            }
            int Result = 0;
            foreach (var item in KitaiskayaTeorema)
            {
                int key = item.Key;
                int value = item.Value;
                int Mi = M / key;
                int MiReverse = ObratnoePoModulu(Mi, key);
                while (MiReverse < 0)
                    MiReverse += key;
                Result += (value * Mi * MiReverse) % M;
            }
            return Result;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int q, a, b;
            Console.WriteLine("x = LOG a (b) (mod q)");
            Console.Write("a = ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.Write("b = ");
            b = Convert.ToInt32(Console.ReadLine());
            Console.Write("q = ");
            q = Convert.ToInt32(Console.ReadLine());    
            Logarifmiravanie task = new Logarifmiravanie(q, a, b);
            //получаем простые множители до числа 100
            List<int> ProstieMnojiteli = task.Eratosfen(1000);
            //раскладываем q-1 на простые множители
            Dictionary<int, int> MnojitelAndStepen = task.RazlojnieNaProstieMnojiteli(ProstieMnojiteli);
            //составляем таблицу p
            Dictionary<int, List<BigInteger>> TablicaP = task.CreateTable(MnojitelAndStepen);
            //вычисление дискретного логарифма
            Dictionary<int, int> KitaiskayaTeorema = task.KitaiskayaTeorema(MnojitelAndStepen, TablicaP);
            //решение китайской теоремы 
            int Result = task.ReshenieKitayskoiTeoremi(KitaiskayaTeorema);
            Console.WriteLine("x = " + Result);
        }
    }
}
