﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace SlepayaPodpis
{
    class Program
    {
        static void poModulu(ref int value, int modulus)
        {
            value %= modulus;
            while (value < 0)
                value += modulus;
        }
        static int obratnoePoModulu (int value, int modulus)
        {
            int N = modulus;
            int q, r, x1, x2, y1, y2, x, y, d;
            if (modulus == 0)
            {
                d = value;
                x = 1;
                y = 0;
                return -1;
            }
            x2 = 1;
            x1 = 0;
            y2 = 0;
            y1 = 1;
            while (modulus > 0)
            {
                q = value / modulus;
                r = value - q * modulus;
                x = x2 - q * x1;
                y = y2 - q * y1;
                value = modulus;
                modulus = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }
            d = value;
            x = x2;
            y = y2;
            poModulu(ref x, N);
            return x;
        }
        static int gcd(int value, int modulus)
        {
            int c;
            while (modulus > 0)
            {
                c = value % modulus;
                value = modulus;
                modulus = c;
            }
            return value;
        }
        static void Main(string[] args)
        {
            //SlepayaPodpis.Go();
            //Console.WriteLine();
            for (int i = 0; i < 10000; i++)
                ParadoksDneyRojdenii.Go();
            Console.WriteLine("Среднее значение повторений (сообщений в мапе), которое требуется для поиска коллизии: " + ParadoksDneyRojdenii.nAverage / 10000.0);
        }
        static class SlepayaPodpis
        {
            public static void Go()
            {
                int e = 31, N = 287, M = 123, d = obratnoePoModulu(e,240), r = 0;
                Random rnd = new Random();
                do
                {
                    r = rnd.Next(N);
                } while (gcd(r, N) != 1);
                BigInteger Sbob = BigInteger.ModPow(M, d, N);
                BigInteger M1 = BigInteger.ModPow(r, e, N) * M % N;
                BigInteger S1 = BigInteger.ModPow(M1, d, N);
                BigInteger Seva = (S1 * obratnoePoModulu(r, N)) % N;
                Console.WriteLine("Слепая подпись");
                Console.WriteLine("Ключ шифрования Боба: " + Sbob);
                Console.WriteLine("Ключ шифрования, который взломама Ева: " + Seva);
            }
        }

        static class ParadoksDneyRojdenii
        {
            static public int nAverage = 0;
            public static ushort GetHashCode(string input)
            {
                int result = input.GetHashCode() % ushort.MaxValue;
                return (ushort)result;
            }
            public static void Go()
            {
                //Console.WriteLine("Атака с помощью парадокса дней рождений");
                Dictionary<ushort, string> hash1 = new Dictionary<ushort, string>();
                Dictionary<ushort, string> hash2 = new Dictionary<ushort, string>();
                string str1 = "подателю сего выдать 1000 рублей";
                string str2 = "подателю сего выдать 1000000 рублей";
                string[] strArray = new string[] { " ", "\n"};
                ushort hashcode1 = GetHashCode(str1);
                ushort hashcode2 = GetHashCode(str2);
                hash1.Add(hashcode1, str1);
                hash2.Add(hashcode2, str2);
                int n = 0;
                Random rnd = new Random();
                while (true)
                {
                    n++;   
                    bool bHash2Contains = hash2.ContainsKey(hashcode1);
                    bool bHash1Contains = hash1.ContainsKey(hashcode2);
                    if (bHash2Contains || bHash1Contains)
                    {
                        ushort key = bHash2Contains ? hashcode1 : hashcode2;
                        //Console.WriteLine("Строка " + hash1[key] + " заменяется строкой " + hash2[key]);
                        //Console.WriteLine("Практическое n : " + n);
                        nAverage += n;
                        return;
                    }
                    string symbol = strArray[rnd.Next() % 2];
                    str1 += symbol;
                    str2 += symbol;
                    hashcode1 = GetHashCode(str1);
                    hashcode2 = GetHashCode(str2);
                    if (!hash1.ContainsKey(hashcode1))
                        hash1.Add(hashcode1, str1);
                    if (!hash2.ContainsKey(hashcode2))
                        hash2.Add(hashcode2, str2);
                }
            }
        }
    }
}
