#include <iostream>
#include <string>
using namespace std;
string g_strInput = "bivllxyvxipbfxygokpi";
int g_N = 26;
int g_aReverse = 451;
int g_b = 11;
string alphabet[26] = { "a","b","c","d","e","f","g","h","i","g","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z" };
// bivllxyvxipbfxygokpi
int findNumber(char cSymbol)
{
	string s(1, cSymbol);
	for (int i = 0; i < 26; i++)
		if (s == alphabet[i])
			return i;
	return -1;
}

string decryption()
{
	//p = c * a^-1 - b* a^-1 (mod N)
	int nCount = g_strInput.length() / 2;
	int c;
	int p;
	string strResult;
	int nLetter1;
	int nLetter2;
	for (int i = 0; i < nCount; i++)
	{
		c = findNumber(g_strInput[i]) * g_N + findNumber(g_strInput[i + 1]);
		p = (c * g_aReverse - g_b * g_aReverse) % (g_N * g_N);
		nLetter1 = p / g_N;
		nLetter2 = p % g_N;
		strResult += alphabet[nLetter1] + alphabet[nLetter2];
	}
	return strResult;
}

void main()
{
	setlocale(LC_ALL, "Rus");
	/*cout << "������� ������������� ������: ";
	cin >> g_strInput;
	cout << "������� ����������� ��������: ";
	cin >> g_N;
	cout << "������� a^-1: ";
	cin >> g_aReverse;
	cout << "������� b: ";
	cin >> g_b;*/
	cout << "������������� ������: " << decryption();
}