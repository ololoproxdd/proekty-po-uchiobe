#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
void main()
{
	ifstream input;
	ofstream output;
	input.open("input.txt");
	int nCount = 0;
	if (!input.eof())
	{
		input >> nCount;
		if (nCount <= 0)
		{
			cout << "error";
			return;
		}
	}
	else
	{
		cout << "error";
		return;
	}
	output.open("output.txt");
	int a = 587594; //8F74A
	for (int i = 0; i < nCount; i++)
	{
		int b = 143473; //23071
		int c = 752418; //B7B22
		int x = a*b + c;
		x %= 0x7FFFFFFF;
		x = (x / 0x100) % 0x8000;
		x += 0x10000;
		a = x;
		output << x << endl;
		cout << x << endl;
	}
	input.close();
	output.close();
}